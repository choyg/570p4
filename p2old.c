/*
Gensen Choy
Carroll
CS570
October 11, 2017 11 PM.
*/

#include "getword.h"
#include "p2.h"
#include <stdlib.h>
#include <signal.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>

void showPrompt();

void printLine(char *line[]);

int parse(char *strings[], char *args[]);

volatile sig_atomic_t done = 0;

void term(int signum) {
    done = 1;
}

const int NORMAL = 0;
const int INPUT = 1;
const int OUTPUT = 2;
const int ARGS = 3;
const int PIPE = 4;
const int BACKGROUND = 1;

int main(int argc, char *argv[]) {

    char *line[MAXITEM];
    char *input;
    char *output;
    char *pipeArg;
    char *args[MAXITEM];
    char *prevArgs[MAXITEM];

    struct sigaction action;
    memset(&action, 0, sizeof(struct sigaction));
    action.sa_handler = term;
    sigaction(SIGTERM, &action, NULL);
    setpgrp();

    for (;;) {

        showPrompt();
        int parseResult = parse(line, args);
        if (parseResult == -1 && line[0] == '\0') {
            break;
        } else if (parseResult >= 0) {
            //assign input/output
            input = line[INPUT];
            output = line[OUTPUT];
            pipeArg = line[PIPE];

            //redirects
            int fd;
            int pastFD = dup(STDOUT_FILENO);
            if (input != NULL) {
                fd = open(input, O_RDONLY);
                if (fd < 0) {
                    perror(input);
                    continue;
                }
                dup2(fd, STDIN_FILENO);
                close(fd);
            } else {  // Set input to /dev/null if no input stream supplied
                open("/dev/null", O_RDONLY);
            }

            if (output != NULL) { // File redirect '>' was used.
                fd = open(output, O_RDWR | O_CREAT | O_EXCL, S_IRUSR | S_IWUSR);
                if (fd < 0) { // <0 err
                    perror(output);
                    continue;
                }
                dup2(fd, STDOUT_FILENO);
                close(fd);
            }

            // handle builtins
            if (strcmp(args[0], "cd") == 0) {
                if (args[1] == NULL) {
                    chdir(getenv("HOME"));
                } else if (chdir(args[1]) != 0) {
                    perror("Error");
                }
                memcpy(prevArgs, args, sizeof(args));
                continue;
            }

            //fun stuff
            fflush(stdout);
            int pid = fork();
            if (pid == 0) { // Child 1

                // pipes - redir output of child 1
                int fildes[2];
                if (pipeArg != NULL) {
                    pipe(fildes);
                    close(fildes[0]);
                    dup2(fildes[1], STDOUT_FILENO);
                    close(fildes[1]);
                }

                setpgrp();
                if (execvp(args[0], args) == -1) {
                    perror("Error");
                    exit(9);
                }


                if (pipeArg != NULL) { // User wants to pipe and run 2nd command
                    close(fildes[1]);
                    dup2(fildes[0], STDIN_FILENO); // Retrieve input from child 1
                    close(fildes[0]);

                    fflush(stdout);
                    int pid2 = fork();
                    if (pid2 == 0) {
                        setpgrp();
                        if (execvp(pipeArg, NULL) == -1) {
                            perror("Error");
                            exit(9);
                        }
                    } else {
                        perror("Error");
                    }
                }
            } else if (pid < 0) {
                perror("Error");
            } else {
                if (parseResult != BACKGROUND) {
                    while (wait(NULL) > 0);
                } else {
                    printf("%s [%d]\n", args[0], pid);
                }
            }

            dup2(pastFD, STDOUT_FILENO);
            close(pastFD);
            memcpy(prevArgs, args, sizeof(args));

        }

    }
    killpg(getpgrp(), SIGTERM);
    printf("p2 terminated. \n");
    exit(0);
}

int parse(char *strings[], char *args[]) {

    char *lastChar = NULL;
    int i;
    int argsLength = 0;
    int status = NORMAL;
    for (i = 0; i <= 4; i++) {
        strings[i] = NULL;
    }
    args[0] = NULL;
    for (i = 0; i < MAXITEM; i++) {
        char *s = malloc(sizeof(char) * STORAGE);
        int length = getword(s);
        if (length == -1) {
            strings[i] = '\0';
            return -1;
        }
        if (length == 0) {
            strings[i] = '\0';
            break;
        } else {
            lastChar = s;
            if (strcmp("|", s) == 0) {
                status = PIPE;
            } else if (strcmp("<", s) == 0) {
                status = INPUT;
            } else if (strcmp(">", s) == 0) {
                status = OUTPUT;
            } else if (status == NORMAL) {
                args[NORMAL] = s;
                status = ARGS;
            } else if (status == PIPE) {
                strings[PIPE] = s;
                if (args[NORMAL] != NULL) status = ARGS;
                else status = NORMAL;
            } else if (status == ARGS) {
                args[1 + argsLength] = s;
                argsLength++;
            } else if (status == INPUT) {
                if (strings[INPUT] != NULL) {
                    perror("ERROR - Multiple Inputs");
                    return -2;
                }
                strings[INPUT] = s;
                if (args[NORMAL] != NULL) status = ARGS;
                else status = NORMAL;
            } else if (status == OUTPUT) {
                if (strings[OUTPUT] != NULL) {
                    perror("ERROR - Multiple Outputs");
                    return -2;
                }
                strings[OUTPUT] = s;
                if (args[NORMAL] != NULL) status = ARGS;
                else status = NORMAL;
            }
        }
    }

    args[1 + argsLength] = '\0'; //terminate string array
    if (lastChar == NULL) {
        return -2;
    }
    if (strcmp("&", lastChar) == 0) {
        args[argsLength] = '\0';
        return BACKGROUND;
    }

    if (args[0] == NULL) {
        printf("%s\n", "Executable not found");
        return -2;
    }
    return 0;
}

void showPrompt() {
    printf("%s", "p2: ");
}

void printLine(char *line[]) {
    int i = 0;
    for (i = 0; line[i]; ++i) {
        const char *word = line[i];
        while (*word) {
            putchar(*word++);
        }
        putchar('\n');
    }
}
