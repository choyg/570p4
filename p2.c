/*
Gensen Choy
Carroll
CS570
October 11, 2017 11 PM.
*/

#include "getword.h"
#include "p2.h"
#include <stdlib.h>
#include <signal.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>

void showPrompt();

void printLine(char *line[]);

int parse(char *strings[], char *args[], char *pipeArgs[][MAXITEM]);

void term(int signum) {}

const int NORMAL = 0;
const int INPUT = 1;
const int OUTPUT = 2;
const int ARGS = 3;
const int PIPE = 4;
const int PIPEARGS = 5;
const int BACKGROUND = 1;

int pipeCount = 0;

/**
 * A very basic shell that supposedly handles redirects, pipes, backgrounding, and several builtins: cd and ls-F.
 *
 * @param argc
 * @param argv
 * @return
 */
int main(int argc, char *argv[]) {

    char *line[MAXITEM];
    char *input;
    char *output;
    char *pipeArg;
    char *args[MAXITEM];
    char *pipeArgs[MAXITEM][MAXITEM];
    struct sigaction action;

    memset(&action, 0, sizeof(struct sigaction));
    action.sa_handler = term;
    sigaction(SIGTERM, &action, NULL); // Custom SIGTERM handler
    setpgrp(); // Set the process group of the shell

    for (;;) {
        showPrompt();
        int parseResult = parse(line, args, pipeArgs);
        pipeArg = pipeArgs[0][0];
        if (parseResult == -1 && line[0] == '\0') {
            break;
        } else if (parseResult >= 0) {
            input = line[INPUT];
            output = line[OUTPUT];

            // Redirects
            int fd;
            int pastFD = dup(STDOUT_FILENO);
            if (input != NULL) {
                fd = open(input, O_RDONLY);
                if (fd < 0) {
                    perror(input);
                    continue;
                }
                dup2(fd, STDIN_FILENO);
                close(fd);
            } else {  // Set input to /dev/null if no input stream supplied
                open("/dev/null", O_RDONLY);
            }

            if (output != NULL) { // File redirect '>' was used.
                // Attempt to open up a file that has Read/Write permissions for the user.
                // By using O_CREATE | O_EXCL flags together, open() will fail if the file already exists.
                fd = open(output, O_RDWR | O_CREAT | O_EXCL, S_IRUSR | S_IWUSR);
                if (fd < 0) { // <0 err
                    perror(output);
                    continue;
                }
                dup2(fd, STDOUT_FILENO);
                close(fd);
            }

            // Builtin cd function attempts to changedir to the HOME directory if it is set
            // Builtin ls-F function replicates tcsh's ls-F by not forking while displaying a list of files
            if (strcmp(args[0], "cd") == 0) {
                if (args[1] == NULL) {
                    chdir(getenv("HOME"));
                } else if (chdir(args[1]) != 0) {
                    perror("Error");
                }
                continue;
            } else if (strcmp(args[0], "ls-F") == 0) {
                DIR *dirp;
                struct dirent *dp;
                char *dirarg = ".";
                if (args[1] != NULL) dirarg = args[1];
                dirp = opendir(dirarg);
                if (dirp) {
                    for (;;) {
                        if ((dp = readdir(dirp)) != NULL) {
                            printf("%s\n", dp->d_name);
                        } else {
                            closedir(dirp);
                            break;
                        }
                    }
                } else {
                    perror("Error trying to open directory");
                }
                continue;
            }


            // Flush before forking to prevent weird output
            fflush(stdout);
            setpgrp(); // Set process group of shell children so it can be reaped
            int pid = fork();
            if (pid == 0) { // Child 1
                if (pipeArg != NULL) {
                    // Flush before forking to prevent weird output
                    fflush(stdout);
                    setpgrp(); // Set process group of shell children so it can be reaped
                    int fildes[4];
                    int pipeInst;
                    //for (pipeInst = 0; pipeInst < pipeCount; pipeInst++) {
                    pipe(fildes);
                    pipe(fildes + 2);
                    //}
                    if (fork() == 0) {
                        // Child process
                        dup2(fildes[0], 0);   /* make stdin same as fildes[0] */
                        dup2(fildes[3], 1);
                        close(fildes[1]);
                        close(fildes[2]);
                        //execvp(pipeArg, pipeArgs[0]);
                        if (fork() == 0) {
                            dup2(fildes[1], 1);
                            close(fildes[0]);
                            close(fildes[2]);
                            close(fildes[3]);
                            //execvp(args[0], args);
                            execlp("ls", "ls", NULL);
                        }
                        execlp("cat", "cat", NULL);


                    } else {
                        dup2(fildes[2], 0);   /* make stdin same as fildes[2] */
                        close(fildes[0]);
                        close(fildes[1]);
                        close(fildes[3]);
                        execlp("grep", "grep", "a", NULL);

                    }
                    close(fildes[0]);
                    close(fildes[1]);
                    close(fildes[2]);
                    close(fildes[3]);
                    int i;
                    int status;
                    for (i = 0; i < 3; i++)
                        wait(&status);

                } else if (execvp(args[0], args) == -1) {
                    perror("Error");
                    exit(9);
                }
            } else if (pid < 0) {
                perror("Error");
            } else {
                if (parseResult != BACKGROUND) {
                    while (wait(NULL) > 0);
                } else {
                    printf("%s [%d]\n", args[0], pid);
                }
            }


            dup2(pastFD, STDOUT_FILENO);
            close(pastFD);
        }

    }
    killpg(getpgrp(), SIGTERM);
    printf("p2 terminated. \n");
    exit(0);
}

/**
 * Reads and parses input from the user. This is supposed to be a useful
 * helper function for extracting data into a structured format
 * @param strings Array that holds meta information for the shell to perform redirects/piping
 * @param args Array that represents an executable call in the 0th index followed by any number of arguments
 * @param pipeArgs Array that represents an executable call that should have data piped into it followed by arguments
 * @return >=0 if the parsing was successful. <0 otherwise.
 */
int parse(char *strings[], char *args[], char *pipeArgs[][MAXITEM]) {

    char *lastChar = NULL;
    int i;
    int argsLength = 0;
    int pipeArgsLength = 0;
    int pipeIndex = -1;
    int status = NORMAL;
    for (i = 0; i <= 4; i++) {
        strings[i] = NULL;
    }
    args[0] = NULL;
    pipeArgs[0][0] = NULL;
    for (i = 0; i < MAXITEM; i++) {
        char *s = malloc(sizeof(char) * STORAGE);
        int length = getword(s);
        if (length == -1) {
            strings[i] = '\0';
            return -1;
        }
        if (length == 0) {
            strings[i] = '\0';
            break;
        } else {
            lastChar = s;
            if (strcmp("|", s) == 0) {
                if (pipeIndex >= 0) {
                    pipeArgs[pipeIndex][pipeArgsLength + 1] = '\0';
                }
                pipeIndex += 1;
                pipeCount += 1;
                pipeArgsLength = 0;
                status = PIPE;
            } else if (strcmp("<", s) == 0) {
                status = INPUT;
            } else if (strcmp(">", s) == 0) {
                status = OUTPUT;
            } else if (status == NORMAL) {
                args[NORMAL] = s;
                status = ARGS;
            } else if (status == PIPE) {
                pipeArgs[pipeIndex][0] = s;
                status = PIPEARGS;
            } else if (status == ARGS) {
                args[1 + argsLength] = s;
                argsLength++;
            } else if (status == PIPEARGS) {
                pipeArgs[pipeIndex][1 + pipeArgsLength] = s;
                pipeArgsLength++;
            } else if (status == INPUT) {
                if (strings[INPUT] != NULL) {
                    perror("ERROR - Multiple Inputs");
                    return -2;
                }
                strings[INPUT] = s;
                if (args[NORMAL] != NULL) status = ARGS;
                else status = NORMAL;
            } else {
                if (strings[OUTPUT] != NULL) {
                    perror("ERROR - Multiple Outputs");
                    return -2;
                }
                strings[OUTPUT] = s;
                if (args[NORMAL] != NULL) status = ARGS;
                else status = NORMAL;
            }
        }
    }
    pipeArgs[pipeIndex][pipeArgsLength + 1] = '\0';
    args[1 + argsLength] = '\0'; //terminate string array
    if (lastChar == NULL) {
        return -2;
    }
    if (strcmp("&", lastChar) == 0) {
        args[argsLength] = '\0';
        return BACKGROUND;
    }

    if (args[0] == NULL) {
        printf("%s\n", "Executable not found");
        return -2;
    }
    return 0;
}

/**
 * A boring prompt generator
 */
void showPrompt() {
    printf("%s", "p2: ");
}

/**
 * Debugging function
 * @param line
 */
void printLine(char *line[]) {
    int i = 0;
    for (i = 0; line[i]; ++i) {
        const char *word = line[i];
        while (*word) {
            putchar(*word++);
        }
        putchar('\n');
    }
}
