cmake_minimum_required(VERSION 3.8)
project(Two)

set(CMAKE_CXX_STANDARD 11)

set(SOURCE_FILES
        getword.c
        getword.h
        makefile
        p2.c
        p2.h)

add_executable(Two ${SOURCE_FILES})