/*
Gensen Choy
Carroll
CS 570 - Operating Systems
9/18/17 (11 pm)
*/
#include <stdio.h>
#include "getword.h"

/*
* The getword() function gets one word from the input stream.
* It returns -1 iff end-of-file is encountered;
* otherwise, it returns the number of characters in the word
*
* INPUT: a pointer to the beginning of a character string
* OUTPUT: -1 or the number of characters in the word
* SIDE EFFECTS: bytes beginning at address w will be overwritten.
*   Anyone using this routine should have w pointing to an
*   available area at least STORAGE bytes long before calling getword().
*/
int getword (char *w)
{
    int currentChar;
    int charCount = 0;
    int isBack = 0;
    int isQuoted = 0;
    int greaterCount = 0;
    int i;
    for (i = 0; i < STORAGE; i++)
    {
        if (charCount >= STORAGE - 1)
        {
            w[charCount] = '\0';
            return charCount;
        }
        currentChar = getchar ();
        // checks for 'logout' keyword and ensures there are no leading/trailing characters
        if (strcmp (w, "logout") == 0 && charCount == 6)
        {
            w[charCount] = '\0';
            return -1;
        }
        if (currentChar == EOF)
        {
            if (charCount > 0)
            {
                // ungetc() is used here and other lines to return a special character back for consumption
                // specifically, it would be the difference between a&f printing a\nf vs. a\n&\nf
                ungetc (EOF, stdin);
                w[charCount] = '\0';
                return charCount;
            }
            w[charCount] = '\0';
            return -1;
        }
        if (currentChar == '\n')
        {
            if (charCount > 0)
            {
                ungetc ('\n', stdin);
            }
            w[charCount] = '\0';
            return charCount;
        }

        // Quoted text should be printed as-is
        if (isQuoted == 1)
        {
            if (w[charCount-1] == '\\' && currentChar == '\'') {
                w[charCount-1] = '\'';
            }
            else if (currentChar == '\'')
            {
                isQuoted = 0;
            }
            else
            {
                w[charCount] = currentChar;
                charCount++;
            }
        }
        // Escaped characters should be printed as-is
        else if (isBack == 1)
        {
            w[charCount] = currentChar;
            charCount++;
            isBack = 0;
        }
        // Set escaped flag
        else if (currentChar == '\\')
        {
            i--;
            isBack = 1;
        }
        // Set quote flaag
        else if (currentChar == '\'')
        {
            i--;
            isQuoted = 1;
        }
        // Check for leading spaces and tabs else use as delimiter
        else if (currentChar == ' ' || currentChar == '\t')
        {
            if (charCount > 0)
            {
                w[charCount] = '\0';
                return charCount;
            }
        }
        else if (greaterCount > 0)
        {
            if (currentChar == '!')
            {
                w[charCount++] = currentChar;
                w[charCount] = '\0';
                return charCount;
            }
            else
            {   //current char is not a ! so we should terminate
                ungetc (currentChar, stdin);
                w[charCount] = '\0';
                return charCount;
            }
        }
        // this branch calls if we had no previous > symbols (excluding backslashed characters)
        else if (currentChar == '>')
        {
            if (charCount > 0 && greaterCount == 0)
            {
                ungetc (currentChar, stdin);
                w[charCount] = '\0';
                return charCount;
            }
            greaterCount++;
            w[charCount++] = currentChar;
        }
        // handles the rest of the metacharacters
        else if (currentChar == '&' || currentChar == '|' || currentChar == '<'
                 || currentChar == '\'')
        {
            if (charCount > 0)
            {
                ungetc (currentChar, stdin);
                w[charCount] = '\0';
                return charCount;
            }
            w[charCount++] = currentChar;
            w[charCount] = '\0';
            return charCount;
        } 
        // Special handling for ; which should be treated like a newline character
        else if (currentChar == ';')
        {
            if (charCount > 0)
            {
                ungetc (currentChar, stdin);
                w[charCount] = '\0';
                return charCount;
            }
            w[charCount] = '\0';
            return charCount;
        }
        else
        {
            w[charCount] = currentChar;
            charCount++;
        }
    }
    return charCount;
}

